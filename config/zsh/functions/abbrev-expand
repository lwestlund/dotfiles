#!/usr/bin/env zsh

# Expand an abbreviation to something long that you don't want to type.

# Abbreviations can be expanded upon by adding key-value-pairs to the `abbrevs`
# associative array, e.g.
#
#   typeset -Ag abbrevs  # Keeps the old abbreviations in the associative array.
#   abbrevs+=("myabbrev" "myexpandedabbreviation")

# To use:
#   autoload -Uz abbrev-expand
#   zle -N abbrev-expand-basic-space abbrev-expand
#   zle -N abbrev-expand-magic-space abbrev-expand
#   zle -N abbrev-expand-and-accept abbrev-expand
#   zle -N abbrev-expand-or-complete abbrev-expand
# and optionally bind to some keys:
#   bindkey "^@" abbrev-expand-basic-space
#   bindkey " " abbrev-expand-magic-space
#   bindkey "^M" abbrev-expand-and-accept
#   bindkey "^I" abbrev-expand-or-complete

autoload -Uz expand-dots
zle -N expand-dots

emulate -L zsh
setopt extendedglob

typeset -Ag abbrevs
abbrevs+=(
    "zource"    "source $ZDOTDIR/.zshrc"

    # Pipes
    "awkp"      "| awk '{print \$__CURSOR__}'"
    "pgr"       "| grep"

    # Git
    "gs"    "git status -sb"
    "gsl"   "git status"
    "g1"    "git log1"
    "g2"    "git log2"
    "g3"    "git log3"

    "ga"    "git add"

    "gc"    "git commit"
    "gcf"   "git commit --fixup"
    "gca"   "git commit --amend"
    "gcane" "git commit --amend --no-edit"

    "gsm"   "git switch -"
    "gsc"   "git switch -c"
    "gsd"   "git switch develop"

    "gb"    "git branch"
    "gbm"   "git branch -M"

    "gd"    "git diff"
    "gdc"   "git diff --cached"
    "gdh"   "git diff HEAD~1"

    "gfo"   "git fetch origin"

    "gp"    "git push"
    "gpf"   "git push --force-with-lease"

    "gl"    "git pull"
    "glr"   "git pull --rebase"

    "grb"   "git rebase"
    "grbi"  "git rebase -i"
    "grbia" "git rebase -i --autosquash"
    "grba"  "git rebase --abort"
    "grbc"  "git rebase --continue"
    "grbm"  "git rebase main"
    "grbd"  "git rebase develop"

    "gr"    "git reset"
    "grs"   "git reset --soft"
    "grh"   "git reset --hard"

    "gcp"   "git cherry-pick"
    "gcpc"  "git cherry-pick --continue"
    "gcpa"  "git cherry-pick --abort"

    "gsh"   "git show"
    "gshs"  "git show --summary"
    "gshh"  "git show HEAD"

    "gsu"   "git submodule update --init --recursive"

    "gsta"  "git stash push"
    "gstd"  "git stash drop"
    "gstl"  "git stash list"
    "gstp"  "git stash pop"
    "gsts"  "git stash show"

    "userctl"   "systemctl --user"
    "usrctl"    "systemctl --user"
)

expand-abbrev() {
    # Docs: http://zsh.sourceforge.net/Doc/Release/Expansion.html#Parameter-Expansion
    # %% : extract the last word from LBUFFER matching a reasonable abbreviation into abbrev.
    local MATCH
    LBUFFER=${LBUFFER%%(#m)[_[:alnum:]]#}
    # Lookup abbrev in abbreviations.
    readonly command=${abbrevs[$MATCH]}
    # If we found a command, add it to LBUFFER, otherwise add $abbrev back.
    LBUFFER+=${command:-$MATCH}

    if [[ "${command}" =~ "__CURSOR__" ]]; then
        # If the expanded abbreviation has a `__CURSOR__`, place us there.
        RBUFFER=${LBUFFER[(ws:__CURSOR__:)2]}
        LBUFFER=${LBUFFER[(ws:__CURSOR__:)1]}
    fi
}

basic-space() {
    LBUFFER+=" "
}

case $WIDGET in
    abbrev-expand-basic-space)
        basic-space
        ;;
    abbrev-expand-magic-space)
        zle expand-dots
        expand-abbrev
        if zle -l autopair-insert; then
            zle autopair-insert
        else
            zle magic-space
        fi
        ;;
    abbrev-expand-and-accept)
        zle expand-dots
        expand-abbrev
        zle accept-line
        ;;
    abbrev-expand-or-complete)
        zle expand-dots
        zle expand-or-complete
        ;;
esac
