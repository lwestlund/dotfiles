{
    "layer": "top", // Waybar at top layer
    "position": "bottom", // Waybar position (top|bottom|left|right)
    // "height": 30, // Waybar height (remove for auto height)
    // "width": 1280, // Waybar width
    "spacing": 4, // Gaps between modules (4px)
    // Choose the order of the modules
    "modules-left": [
                     "hyprland/workspaces",
                     "hyprland/submap",
                     "keyboard-state",
                    ],
    "modules-center": [],
    "modules-right": [
                      // "mpd",
                      // "idle_inhibitor",
                      "tray",
                      "custom/notification",
                      // "pulseaudio",
                      "wireplumber",
                      "network",
                      // "cpu",
                      // "memory",
                      "temperature",
                      // "backlight",
                      "hyprland/language",
                      "battery",
                      "clock",
                     ],
    // Modules configuration
    "hyprland/language": {
        "format": "{}",
        "format-en": "us",
        "format-sv": "se",
    },
    "hyprland/workspaces": {
        "format": "{id}",
        // "format-icons": {
        //     "1": "",
        //     "2": "",
        //     "3": "",
        //     "4": "",
        //     "5": "",
        //     "urgent": "",
        //     "focused": "",
        //     "default": ""
        // }
    },
    "keyboard-state": {
        "capslock": true,
        "format": "{icon}",
        "format-icons": {
            "locked": "caps lock",
            "unlocked": ""
        }
    },
    "sway/mode": {
        "format": "<span style=\"italic\">{}</span>"
    },
    "sway/scratchpad": {
        "format": "{icon} {count}",
        "show-empty": false,
        "format-icons": ["", ""],
        "tooltip": true,
        "tooltip-format": "{app}: {title}"
    },
    // "mpd": {
    //     "format": "{stateIcon} {consumeIcon}{randomIcon}{repeatIcon}{singleIcon}{artist} - {album} - {title} ({elapsedTime:%M:%S}/{totalTime:%M:%S}) ⸨{songPosition}|{queueLength}⸩ {volume}% ",
    //     "format-disconnected": "Disconnected ",
    //     "format-stopped": "{consumeIcon}{randomIcon}{repeatIcon}{singleIcon}Stopped ",
    //     "unknown-tag": "N/A",
    //     "interval": 2,
    //     "consume-icons": {
    //         "on": " "
    //     },
    //     "random-icons": {
    //         "off": "<span color=\"#f53c3c\"></span> ",
    //         "on": " "
    //     },
    //     "repeat-icons": {
    //         "on": " "
    //     },
    //     "single-icons": {
    //         "on": "1 "
    //     },
    //     "state-icons": {
    //         "paused": "",
    //         "playing": ""
    //     },
    //     "tooltip-format": "MPD (connected)",
    //     "tooltip-format-disconnected": "MPD (disconnected)"
    // },
    "idle_inhibitor": {
        "format": "{icon}",
        "format-icons": {
            "activated": "",
            "deactivated": ""
        }
    },
    "tray": {
        // "icon-size": 21,
        "spacing": 10
    },
    "clock": {
        // "timezone": "America/New_York",
        "format": "{:%a %b %d, %H:%M}",
        "tooltip-format": "<big>{:%Y %B}</big>\n<tt><small>{calendar}</small></tt>",
        // "format-alt": "{:%Y-%m-%d}"
    },
    "cpu": {
        "format": "{usage}% ",
        "tooltip": false
    },
    "memory": {
        "format": "{}% "
    },
    "temperature": {
        // Check thermal zone with `for i in /sys/class/thermal/thermal_zone*; do echo "$i: $(<$i/type)"; done`.
        "thermal-zone": 6,
        "input-filename": "temp",
        "critical-threshold": 80,
        "format": "{temperatureC}°C",
        "format-icons": ["", "", "", "", ""]
    },
    "backlight": {
        // "device": "acpi_video1",
        "format": "{percent}% {icon}",
        "format-icons": ["", "", "", "", "", "", "", "", ""]
    },
    "battery": {
        "bat": "BAT0",
        "interval": 60,
        "states": {
            // "good": 95,
            "warning": 30,
            "critical": 20,
        },
        "format": "{icon}",
        "format-charging": "{icon} 󱐋",
        "format-plugged": "󱐋",
        //"format-alt": "{time} {icon}",
        "tooltip-format": "{capacity} %, {timeTo}",
        // "format-good": "", // An empty format will hide the module
        // "format-full": "",
        "format-icons": ["", "", "", "", ""],
    },
    "network": {
        // "interface": "wlp2*", // (Optional) To force the use of this interface
        "format-wifi": "{icon}",
        "format-icons": ["󰤯", "󰤟", "󰤢", "󰤥", "󰤨"],
        "tooltip-format": "{essid} {signalStrength} %",
        "format-linked": "{ifname} (No IP) ",
        "format-disconnected": "󰤮",
        "format-alt": "{essid}: {ipaddr}/{cidr}"
    },
    "pulseaudio": {
        // "scroll-step": 1, // %, can be a float
        "format": "{icon} {format_source}",
        "format-bluetooth": "{icon} {format_source}",
        "format-bluetooth-muted": "{icon} 󰖁 {format_source}",
        "format-muted": "󰝟 {format_source}",
        "format-source": "",
        "format-source-muted": "",
        "format-icons": {
            "headphone": "",
            "hands-free": "",
            "headset": "󰋎 ",
            "phone": "",
            "portable": "",
            "car": "",
            "default": ["󰕿", "󰖀", "󰕾"]
        },
        "on-click": "pwvucontrol",
        "tooltip-format": "{desc} {volume}%"
    },
    "wireplumber": {
        "format": "{icon} {volume}%",
        "format-muted": "󰝟",
        "on-click": "pwvucontrol",
        "on-click-right": "qpwgraph",
        "format-icons": ["󰕿", "󰖀", "󰕾"]
    },
    "custom/media": {
        "format": "{icon} {}",
        "return-type": "json",
        "max-length": 40,
        "format-icons": {
            "spotify": "",
            "default": "🎜"
        },
        "escape": true,
        "exec": "$HOME/.config/waybar/mediaplayer.py 2> /dev/null" // Script in resources folder
        // "exec": "$HOME/.config/waybar/mediaplayer.py --player spotify 2> /dev/null" // Filter player based on name
    },
    "custom/notification": {
        "tooltip": false,
        "format": "{icon}",
        "format-icons": {
            "notification": "<span foreground='red'><sup></sup></span>",
            "none": "",
            "dnd-notification": "<span foreground='red'><sup></sup></span>",
            "dnd-none": "",
            "inhibited-notification": "<span foreground='red'><sup></sup></span>",
            "inhibited-none": "",
            "dnd-inhibited-notification": "<span foreground='red'><sup></sup></span>",
            "dnd-inhibited-none": ""
        },
        "return-type": "json",
        "exec-if": "which swaync-client",
        "exec": "swaync-client --subscribe-waybar",
        "on-click": "swaync-client --toggle-panel --skip-wait",
        "on-click-right": "swaync-client --toggle-dnd --skip-wait",
        "escape": true
    }
}

